window.onload = function(){
    canv = document.getElementById("gc");
    ctx = canv.getContext("2d");
    init();
    canv.addEventListener("click", click);
    
}
//block size : 50 * 50
bs = 50;
//board size : 250 * 250
boardSize = 250;
block = []


function init(){
    //Read Problem
    var prob = readProblem();
    //Draw Number
    drawNumber(prob);

    //Draw Block
    ctx.fillStyle="black"
    ctx.fillRect(250, 250, boardSize, boardSize);
    for(var i = 0 ; i < 5 ; i ++){
        var tmp = [];
        for(var j = 0 ; j < 5 ; j++){
            var b = {x:j, y:i, sol:prob[i][j], state:0};
            tmp.push(b);
            drawBlock(b, "white");
            // b.addEventListener("click", blockClick);
            

        }
        block.push(tmp);
    }
}

function drawNumber(prob){
    ctx.font = "30px Arial";
    ctx.fillStyle="black";
    var al = 15;
    //draw col
    for(var col = 0 ; col < prob[0].length ; col++){
        
        var probNum = getProbNum(prob.map(function(value, idx){return value[col]}));
        console.log(col, {probNum});
        if(probNum.length > 0){
            var cx = 250 + col * 50 + al
            var startY = 250 - (probNum.length * 50) + 30; 
            for(var i = 0 ; i < probNum.length ; i++){
                ctx.fillText(probNum[i], cx, startY + i*50);
                console.log(cx, startY+i*50);
            }
        }
    }

    //draw row
    for(var row = 0 ; row < prob.length ; row++){
        
        var probNum = getProbNum(prob[row]);
        console.log(row, {probNum});
        if(probNum.length > 0){
            var cy = 250 + row * 50 + 30;
            var startX = 250 - (probNum.length * 50) + 15; 
            console.log({cy}, {startX});
            for(var i = 0 ; i < probNum.length ; i++){
                ctx.fillText(probNum[i], startX + i*50, cy);
                // console.log(cx, startY+i*50);
            }
        }
    }
    
}

function getProbNum(arr){
    var result = [];
    var flag = false;
    var tmp = 0;
    for(var i = 0 ; i < arr.length ; i++){

        if(arr[i]==0){
            if(flag){
                flag = false;
                result.push(tmp);
                tmp = 0;
            }

        }else{
            if(!flag){
                flag = true;
            }
            tmp++;
        }
        // console.log({i}, arr[i], {tmp}, {flag}, {result}); 

    }
    if(flag){
        result.push(tmp);
    }
    return result;

}

function readProblem(){
    console.log("readProblem called");
    return [[1, 1, 1, 1, 0], 
            [0, 0, 0, 0, 1],
            [0, 1, 0, 0, 1],
            [0, 0, 1, 0, 1],
            [0, 0, 1, 0, 1], 
           ];
}

function drawBlock(b, color){
    ctx.fillStyle=color;
    
    ctx.fillRect(bs*b.x+1 + boardSize, bs*b.y+1 + boardSize, bs-2 , bs-2);
    // console.log({b});
}

function click(event){
    console.log({event});
    validateBlock(event.clientX-boardSize, event.clientY-boardSize);
}

function validateBlock(x, y){
    i = Math.floor(y / bs);
    j = Math.floor(x / bs);
    console.log({i, j});
    if( j >= block.length || i >= block[0].length ){
        console.log("out of canvas");
        return;
    }

    b = block[i][j];
    if(b.sol == 1){
        drawBlock(b, "#333333");
    }else{
        drawBlock(b, "red");
        setTimeout(function(){drawBlock(b, "white")}, 1000);
    }

}

